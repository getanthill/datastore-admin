FROM node:16 AS base

# Create app directory
WORKDIR /app

# ---- Dependencies ----
FROM base AS dependencies
ARG NPM_TOKEN=
ARG BUILD_ENV=production

COPY package*.json ./
RUN npm ci

# ---- Copy Files/Build ----
FROM dependencies AS build
WORKDIR /app
ARG NPM_TOKEN=

COPY tsconfig* package.json svelte.config.js postcss.config.cjs tailwind.config.cjs ./
COPY src /app/src
COPY static /app/static
RUN npm run build

# --- Release with Slim ----
FROM node:16-slim AS release
WORKDIR /app

COPY package.json /app
COPY --from=dependencies /app/node_modules node_modules
COPY --from=build /app/build build
COPY --from=build /app/.svelte-kit .svelte-kit

COPY svelte.config.js /app/svelte.config.js

ARG NODE_ENV=production

CMD ["npx", "svelte-kit", "preview", "--port", "${PORT}", "--host"]
