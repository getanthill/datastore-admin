TAG := `cat package.json | jq -r '.version'`

publish: publish_npm publish_docker

publish_npm:
	npm run build
	npm version patch
	git push --follow-tags
	npm publish

publish_docker:
	docker build --build-arg NPM_TOKEN=${NPM_TOKEN} . --tag datastore-admin

	docker tag datastore-admin:latest getanthill/datastore-admin:$(TAG)
	docker push getanthill/datastore-admin:$(TAG)

	docker tag datastore-admin:latest getanthill/datastore-admin:latest
	docker push getanthill/datastore-admin:latest
 