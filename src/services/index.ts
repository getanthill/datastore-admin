import omit from 'lodash/omit.js';

import stores from './stores';

export interface Services {
	stores: any;
	stringify: Function;
	parse: Function;
	sanitize: Function;
}

const services: Services = {
	stores,
	stringify: (value) => JSON.stringify(value, null, 2),
	parse: (str) => JSON.parse(str),
	sanitize: (obj) => omit(obj, 'created_at', 'updated_at', 'version', '_id')
};

export { stores, services };
