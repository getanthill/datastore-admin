import type DatastoreI from '@getanthill/datastore/dist/sdk/Datastore.js';

import { derived, writable } from 'svelte/store';

import c from '@getanthill/datastore/dist/sdk/Datastore.js';

// @ts-ignore
const Datastore = c.default ? c.default : c;

const DATASTORE_CONFIGS_PATH = 'configs';
let storedConfigs = '[{ "base_url": "http://localhost:3001", "access_token": "token" }]';
export const configs = writable(JSON.parse(storedConfigs));

const DATASTORE_ACTIVE_CONFIG_INDEX_PATH = 'active_config_index';
let storedActiveConfigIndex = 0;
export const activeConfigIndex = writable(storedActiveConfigIndex);
export const activeConfig = derived(
	[configs, activeConfigIndex],
	([$configs, $activeConfigIndex]) => {
		console.log('[store] Update activeConfig');
		return $configs[$activeConfigIndex];
	}
);

export const activeClient = derived<any, DatastoreI>([activeConfig], ([$activeConfig]) => {
	console.log('[store] Update activeClient');
	if (!$activeConfig) {
		return;
	}

	const client = new Datastore({
		baseUrl: $activeConfig.base_url,
		token: $activeConfig.access_token
	});

	return client;
});

// @ts-ignore
export const models = derived([activeClient], async ([$activeClient], set) => {
	console.log('[store] Update models');

	set({});
	if (!$activeClient) {
		return;
	}

	try {
		const { data } = await $activeClient.getModels();

		set(data);
	} catch (err) {
		console.error(err.message);
		// console.error(err);
	}
});

export const filterModels = writable('');

const EDITOR_VALUE_PATH = 'editor_value';
let storedEditorValue = '';
export const editorValue = writable(storedEditorValue);

const stores = {
	models,
	configs,
	activeConfigIndex,
	activeConfig,
	activeClient,
	filterModels,
	editorValue
};

// ---

if (typeof window !== 'undefined') {
	storedConfigs = JSON.parse(localStorage.getItem(DATASTORE_CONFIGS_PATH) || storedConfigs);
	configs.set(storedConfigs);
	configs.subscribe((value) => {
		localStorage.setItem(DATASTORE_CONFIGS_PATH, JSON.stringify(value));
	});

	storedActiveConfigIndex = parseInt(
		`${localStorage.getItem(DATASTORE_ACTIVE_CONFIG_INDEX_PATH) || '0'}`,
		10
	);
	activeConfigIndex.set(storedActiveConfigIndex);
	activeConfigIndex.subscribe((value) => {
		localStorage.setItem(DATASTORE_ACTIVE_CONFIG_INDEX_PATH, `${value}`);
	});

	storedEditorValue = localStorage.getItem(EDITOR_VALUE_PATH) || '';
	editorValue.set(storedEditorValue);
	editorValue.subscribe((value) => {
		localStorage.setItem(EDITOR_VALUE_PATH, `${value}`);
	});
}

export default stores;
