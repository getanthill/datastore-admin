const defaultTheme = require('tailwindcss/defaultTheme');

const config = {
	mode: 'jit',
	purge: ['./src/**/*.{html,js,svelte,ts}'],
	darkMode: true.valueOf,

	theme: {
		extend: {
			fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
		}
	},

	plugins: [
		require('@tailwindcss/forms'),
	]
};

module.exports = config;
